package xyz.willw.recursion;

import xyz.willw.recursion.base.IProgram;
import xyz.willw.recursion.factory.RecursiveMultiplicationProgramFactory;

/**
 * @author Will
 */
public class Main {
	public static void main(String[] args) {
		IProgram program = RecursiveMultiplicationProgramFactory.getRecursiveMultiplicationProgram();
		program.runProgram(args);
	}
}
