package xyz.willw.recursion.base;

/**
 * @author Will
 */
public interface IInputStrategy {
	String next();
}
