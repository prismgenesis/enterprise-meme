package xyz.willw.recursion.base;

/**
 * @author Will
 */
public interface IOutputStrategy {
	void output(String s);
	void error(String s);
}
