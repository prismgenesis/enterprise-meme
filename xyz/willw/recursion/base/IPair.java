package xyz.willw.recursion.base;

/**
 * @author Will
 */
public interface IPair<A, B> {
	A first();
	B second();
}
