package xyz.willw.recursion.base;

/**
 * @author Will
 */
public interface IParser<T> {
	T parse(String from);
}
