package xyz.willw.recursion.base;

/**
 * @author Will
 */
public interface IProgram {
	String getProgramName();
	void runProgram(String... args);
}
