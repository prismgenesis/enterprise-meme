package xyz.willw.recursion.base;

/**
 * @author Will
 */
public interface IRecursive<T> {
	int getRepetitions();
	T execute();
}
