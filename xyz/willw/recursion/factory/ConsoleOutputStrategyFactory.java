package xyz.willw.recursion.factory;

import xyz.willw.recursion.impl.ConsoleOutputStrategy;

/**
 * @author Will
 */
public class ConsoleOutputStrategyFactory {
	private ConsoleOutputStrategyFactory() { }

	public static ConsoleOutputStrategy getConsoleOutputStrategy() {
		return new ConsoleOutputStrategy(System.out);
	}
}
