package xyz.willw.recursion.factory;

import xyz.willw.recursion.impl.IntegerPair;
import xyz.willw.recursion.impl.IntegerParser;
import xyz.willw.recursion.impl.StringPair;

/**
 * @author Will
 */
public class IntegerPairFactory {
	private IntegerPairFactory() { }

	public static IntegerPair getIntegerPair(StringPair stringPair) {
		IntegerParser parser = IntegerParserFactory.getIntegerParser();
		return new IntegerPair(parser.parse(stringPair.first()), parser.parse(stringPair.second()));
	}

	public static IntegerPair getFallbackIntegerPair() {
		return new IntegerPair(5, 2);
	}
}
