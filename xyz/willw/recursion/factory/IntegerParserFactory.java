package xyz.willw.recursion.factory;

import xyz.willw.recursion.impl.IntegerParser;

/**
 * @author Will
 */
public class IntegerParserFactory {
	private IntegerParserFactory() { }

	public static IntegerParser getIntegerParser() {
		return new IntegerParser();
	}
}
