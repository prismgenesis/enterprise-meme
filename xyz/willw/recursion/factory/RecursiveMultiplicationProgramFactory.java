package xyz.willw.recursion.factory;

import xyz.willw.recursion.impl.program.RecursiveMultiplicationProgram;

/**
 * @author Will
 */
public class RecursiveMultiplicationProgramFactory {
	private RecursiveMultiplicationProgramFactory() { }

	public static RecursiveMultiplicationProgram getRecursiveMultiplicationProgram() {
		return new RecursiveMultiplicationProgram("Multiplication Wizard",
				ConsoleOutputStrategyFactory.getConsoleOutputStrategy(),
				ScannerInputStrategyFactory.getScannerInputStrategy());
	}
}
