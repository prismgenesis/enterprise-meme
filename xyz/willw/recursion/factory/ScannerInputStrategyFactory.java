package xyz.willw.recursion.factory;

import xyz.willw.recursion.impl.ScannerInputStrategy;

import java.util.Scanner;

/**
 * @author Will
 */
public class ScannerInputStrategyFactory {
	private ScannerInputStrategyFactory() { }

	public static ScannerInputStrategy getScannerInputStrategy() {
		return new ScannerInputStrategy(new Scanner(System.in));
	}
}
