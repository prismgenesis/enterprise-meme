package xyz.willw.recursion.factory;

import xyz.willw.recursion.base.IInputStrategy;
import xyz.willw.recursion.impl.StringPair;

/**
 * @author Will
 */
public class StringPairFactory {
	private StringPairFactory() { }

	public static StringPair getStringPair(String[] arguments) {
		return new StringPair(arguments[0], arguments[1]);
	}

	public static StringPair getStringPair(String one, String two) {
		return new StringPair(one, two);
	}

	public static StringPair getStringPair(IInputStrategy inputStrategy) {
		return new StringPair(inputStrategy.next(), inputStrategy.next());
	}
}
