package xyz.willw.recursion.impl;

import xyz.willw.recursion.base.IRecursive;

/**
 * @author Will
 */
public class AdditionRecursive implements IRecursive<Integer> {
	private int repetitions;
	private final int value;

	public AdditionRecursive(int value, int repetitions) {
		this.value = value;
		this.repetitions = repetitions;
	}

	@Override public int getRepetitions() {
		return repetitions;
	}

	@Override public Integer execute() {
		if(repetitions == 0)
			return 0;
		repetitions--;
		return value + execute();
	}
}
