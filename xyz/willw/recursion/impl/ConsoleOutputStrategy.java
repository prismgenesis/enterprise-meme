package xyz.willw.recursion.impl;

import xyz.willw.recursion.base.IOutputStrategy;

import java.io.PrintStream;

/**
 * @author Will
 */
public class ConsoleOutputStrategy implements IOutputStrategy {
	private final PrintStream printStream;

	public ConsoleOutputStrategy(PrintStream printStream) {
		this.printStream = printStream;
	}

	@Override public void output(String s) {
		printStream.println(s);
	}

	@Override public void error(String s) {
		printStream.println("[Error]: " + s);
	}
}
