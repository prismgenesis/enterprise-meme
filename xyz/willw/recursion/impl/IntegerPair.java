package xyz.willw.recursion.impl;

import xyz.willw.recursion.base.IPair;

/**
 * @author Will
 */
public class IntegerPair implements IPair<Integer, Integer> {
	private final Integer first, second;

	public IntegerPair(Integer first, Integer second) {
		this.first = first;
		this.second = second;
	}

	@Override public Integer first() {
		return first;
	}

	@Override public Integer second() {
		return second;
	}
}
