package xyz.willw.recursion.impl;

import xyz.willw.recursion.base.IParser;

/**
 * @author Will
 */
public class IntegerParser implements IParser<Integer> {
	@Override public Integer parse(String from) {
		return Integer.parseInt(from);
	}
}
