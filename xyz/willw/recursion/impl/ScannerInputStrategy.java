package xyz.willw.recursion.impl;

import xyz.willw.recursion.base.IInputStrategy;

import java.util.Scanner;

/**
 * @author Will
 */
public class ScannerInputStrategy implements IInputStrategy {
	private final Scanner scanner;

	public ScannerInputStrategy(Scanner scanner) {
		this.scanner = scanner;
	}

	@Override public String next() {
		return scanner.nextLine();
	}
}
