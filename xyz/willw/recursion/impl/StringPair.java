package xyz.willw.recursion.impl;

import xyz.willw.recursion.base.IPair;

/**
 * @author Will
 */
public class StringPair implements IPair<String, String> {
	private final String first, second;

	public StringPair(String first, String second) {
		this.first = first;
		this.second = second;
	}

	@Override public String first() {
		return first;
	}

	@Override public String second() {
		return second;
	}
}
