package xyz.willw.recursion.impl.program;

import xyz.willw.recursion.base.IInputStrategy;
import xyz.willw.recursion.base.IOutputStrategy;
import xyz.willw.recursion.base.IProgram;
import xyz.willw.recursion.base.IRecursive;
import xyz.willw.recursion.factory.IntegerPairFactory;
import xyz.willw.recursion.factory.StringPairFactory;
import xyz.willw.recursion.impl.AdditionRecursive;
import xyz.willw.recursion.impl.IntegerPair;

/**
 * @author Will
 */
public class RecursiveMultiplicationProgram implements IProgram {
	private final String programName;
	private final IOutputStrategy outputStrategy;
	private final IInputStrategy  inputStrategy;

	public RecursiveMultiplicationProgram(String programName, IOutputStrategy outputStrategy, IInputStrategy inputStrategy) {
		this.programName = programName;
		this.outputStrategy = outputStrategy;
		this.inputStrategy = inputStrategy;
	}

	@Override public String getProgramName() {
		return programName;
	}

	@Override public void runProgram(String... args) {
		this.outputStrategy.output(this.getProgramName());
		IntegerPair pair;
		try {
			 pair = IntegerPairFactory.getIntegerPair(StringPairFactory.getStringPair(args));
		} catch (Exception e) {
			try {
				outputStrategy.output("Input two integers, each followed by enter:");
				pair = IntegerPairFactory.getIntegerPair(StringPairFactory.getStringPair(inputStrategy));
			} catch (Exception e1) {
				outputStrategy.error("There was a problem with your input. Using fallback. (5 x 2)");
				pair = IntegerPairFactory.getFallbackIntegerPair();
			}
		}
		IRecursive<Integer> recursive = new AdditionRecursive(pair.first(), pair.second());
		outputStrategy.output("The output is: " + recursive.execute());
	}
}
